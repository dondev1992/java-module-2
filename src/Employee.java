/**
 * @Assignment - Java Programming Activity 2
 * @Class - CO-CP Alpha 1
 * @author - Don Moore
 * @Task - Create an app that read employee objects and determine if they are eligible for a promotion or salary bonus
 *       - Output 2 sentences per employee
 *       - First sentence should describe the current status of the employee
 *       - Second sentence to describe the employee, their current level, their new level and their new salary or their bonus amount
 */


public class Employee {

    // Create Employee class
    private String name;
    private int level;
    private String role;
    private int salary;

    // Create Employee Constructor
    Employee(String name, int level, String role, int salary) {
        this.name = name;
        this.level = level;
        this.role = role;
        this.salary = salary;
    }

    // Create getter and setter functions
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    // Function that displays employee description
    public void  displayEmployeeDescription() {
        if (level == 0) {
            System.out.println("Employee " + name + " is a " + role + " who earns an annual salary of $" + salary + ".");
        } else {
            System.out.println("Employee " + name + " is a Level " + level + " " + role + " who earns an annual salary of $" + salary + ".");
        }
    }
} // End Class
