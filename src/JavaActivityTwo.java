/**
 * @Assignment - Java Programming Activity 2
 * @Class - CO-CP Alpha 1
 * @author - Don Moore
 * @Task - Create an app that read employee objects and determine if they are eligible for a promotion or salary bonus
 *       - Output 2 sentences per employee
 *       - First sentence should describe the current status of the employee
 *       - Second sentence to describe the employee, their current level, their new level and their new salary or their bonus amount
 */
public class JavaActivityTwo {
    public static void printPromotionResults(int currentLevel, String name, String role, int newSalary, String newRole) {
        if (currentLevel > 0) {
            System.out.println(name + " has been promoted from level " + currentLevel + " to level " + (currentLevel + 1) + " with a new annual salary of $" + newSalary + ".\n");
        }  else if (role == "Project Manager"){
            System.out.println(name + " has been promoted from " + role + " to " + newRole + " with a new annual salary of $" + newSalary + ".\n");
        }
    }
    public static void checkIfEligibleForPromotion(Employee[] array) {

        for (Employee employee: array) {

            // Print employee description sentence
            employee.displayEmployeeDescription();

            int currentLevel = employee.getLevel();
            String name = employee.getName();
            String role = employee.getRole();
            String newRole = employee.getRole();
            int newSalary;

            // Check if eligible for promotion or bonus
            if (employee.getLevel() != 0) {
                if (currentLevel == 1) {
                    employee.setLevel(2);
                    employee.setSalary(100000);
                    newSalary = employee.getSalary();

                    printPromotionResults(currentLevel, name, role, newSalary, newRole);

                } else if (currentLevel == 2) {
                    employee.setLevel(3);
                    employee.setSalary(120000);
                    newSalary = employee.getSalary();

                    printPromotionResults(currentLevel, name, role, newSalary, newRole);

                } else {
                    System.out.println(employee.getName() + " has received a bonus of $12000.\n");
                }
            } else if (employee.getRole() == "Project Manager") {
                role = employee.getRole();
                employee.setRole("Senior Project Manager");
                employee.setSalary(150000);
                newRole = employee.getRole();
                newSalary = employee.getSalary();

                printPromotionResults(currentLevel, name, role, newSalary, newRole);

            } else {
                System.out.println(employee.getName() + " has received a bonus of $30,000.\n");
            }
        }
    }
    public static void main(String[] args) {
        // Create employee instances from Employee class
        Employee employee1 = new Employee("Jane Doe", 3, "Software Engineer", 120000);
        Employee employee2 = new Employee("John Smith", 1, "Software Engineer", 85000);
        Employee employee3 = new Employee("Tracy Johnson", 0, "Project Manager", 120000);

        // Put all employee instances in an array
        Employee[] employees = {employee1, employee2, employee3};

        checkIfEligibleForPromotion(employees);
    }
} // End Class